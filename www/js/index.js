/*
 * Created by B00102152.
 */


/* Below we declare some variables used in the application.*/

var entry;

var employeenameField, datetimeField, locationField, categoryField, notesField, contactnoField, imageURI = "";
var resetButton, submitButton;
var thumbnail;
var currentDateTime;
var sortList;

// Variables for faults list
var divID = 0;
var cbuttonID = 0;
var dbuttonID = 1000;

var myControls = [];

// Fault constructor
var Fault = function(employeename, datetime, location, category, notes, contactno, image) {
    this.employeename = employeename;
    this.datetime = datetime;
	this.location = location;
    this.category = category;
    this.notes = notes;
    this.contactno = contactno;
    this.image = image;
};

// Set up the array to store our faults.

var faultsArray = [];

/**
 * Function to convert the date into a readable
 * string.
 * @returns {string}
 */

Fault.prototype.getDate = function(){
    return this.dob.toDateString() ;
};

/**
 * Function to capitalise the first letter of a string and return
 * the result.
 * @returns {string}
 */

String.prototype.capitalize = function() {
    if (this.String() !== undefined)
    return this.charAt(0).toUpperCase() + this.slice(1);
};


var addFault = function(employeenameField, datetimeField, locationField, categoryField, notesField, contactnoField) {

    entry = new Fault(employeenameField.value, datetimeField.value, locationField.value, categoryField.value, notesField.value, contactnoField.value, imageURI);
    faultsArray.push(entry);
};

var add = function () {
    addFault(employeenameField, datetimeField, locationField, categoryField, notesField, contactnoField);
    clearUI();
    saveFaultList();
};

// Function to clear all UI fields.

var clearUI = function() {

    employeenameField.value = "";
	locationField.value = "";

	// Reset the category options list
    var categorySelect = $("#category");
    categorySelect[0].selectedIndex = 0;
    categorySelect.selectmenu("refresh");

    notesField.value = "";
    contactnoField.value = "";

    // Clear any image
    imageURI = "";
    thumbnail.src = "";

    // Reset time
    currentDateTime = new Date($.now()).toString();
    currentDateTime = currentDateTime.slice(0,25);
    $('#datetime').val(currentDateTime);
};


window.onload = function(){

    if (localStorage.getItem("sortPref") === null) {
        localStorage.setItem("sortPref", "oldest");
        sortList = "oldest";
    } else {
        sortList = localStorage.getItem("sortPref");
    }

    // Set sort radio buttons to correct state
    if (sortList === "oldest") {
        $("#sortOldButton").prop("checked", true)
    } else {
        $("#sortNewButton").prop("checked", true)
    }

    employeenameField = document.getElementById("employeename");
    datetimeField = document.getElementById("datetime");
	locationField = document.getElementById("location");
    categoryField = document.getElementById("category");
    notesField = document.getElementById("notes");
    contactnoField = document.getElementById("contactno");

    resetButton = document.getElementById("reset");
    resetButton.onclick = clearUI;

    thumbnail = document.getElementById('imgThumb');

	submitButton = document.getElementById("submit");
	// Submit button clicked
    submitButton.onclick = function(){

        var response = confirm("Add this fault to the list?");

        if (response) {
            add();
            updateList();
            alert("Fault added.");
		}
	};

    //Prepare fault list
    loadFaultList();
    updateList();

};

function saveFaultList() {
    var faultArrayString = JSON.stringify(faultsArray);
    if(faultArrayString !== ""){
        localStorage.faults = faultArrayString;
    } else {
        alert("Could not save the fault at this time");
    }
}

var loadFaultList = function() {
    var faultArrayString = "";
    if(localStorage.faults !== undefined) {
        faultArrayString = localStorage.faults;
        faultsArray = JSON.parse(faultArrayString);
		var proto = new Fault();
		for(var i=0;i<faultsArray.length;i++) {
            var flt = faultsArray[i];
			flt.__proto__ = proto;
		}
	}
};

// onGpsSuccess Callback
// This method accepts a Position object, which contains the
// current GPS coordinates
//
var onGpsSuccess = function(position) {
    $("#location").val("loc:" + position.coords.latitude + "+" + position.coords.longitude);
};

// onGpsError Callback receives a PositionError object
//
function onGpsError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

// Camera success
function onCameraSuccess(imageData) {
    console.log(JSON.stringify(imageData));
    imageURI = imageData;

    thumbnail.src = imageData;
}

// Camera error
function onCameraError(message) {
    alert('Failed because: ' + message);
}


$(document).ready(function(){


    // Get the current date and time and shorten it to a relevant string
    currentDateTime = new Date($.now()).toString();
    currentDateTime = currentDateTime.slice(0,24);
    $('#datetime').val(currentDateTime);


   // When geoButton is clicked
    $("#geoButton").click(function () {
        navigator.geolocation.getCurrentPosition(onGpsSuccess, onGpsError);
    });

    // When the camera button is clicked
	$("#cameraButton").click(function () {
        navigator.camera.getPicture( onCameraSuccess, onCameraError, {quality: 50, destinationType: Camera.DestinationType.FILE_URI, targetWidth: 800, targetHeight: 800  } );
    });

    // When the sortNewButton is clicked
    $("#sortNewButton").click(function () {
        localStorage.setItem("sortPref", "newest")
        updateList();
    });

    // When the sortOldButton is clicked
    $("#sortOldButton").click(function () {
        localStorage.setItem("sortPref", "oldest")
        updateList();
    });
});





// Faults.js import

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};


/**
 * Create divs to show all of the recorded faults stored in localStorage.
 */
Fault.prototype.divRow = function(){

    divID++;
    cbuttonID--;
    dbuttonID++;
    var control = [];
    control[0] = divID;
    control[1] = cbuttonID;
    var div;
    var imgTag, locTag;
    myControls.push(control);

    if (this.employeename === "") {

        this.employeename = "-";
    }


    if (this.datetime == "") {

        this.datetime = "-";
    }

    if (this.category === "" ) {

        this.category = "No category";
    }

    if (this.notes === ""){

        this.notes = "-";
    }

    if (this.contactno === "") {

        this.contactno = "-";
    }

    if (this.location === "") {
        locTag = " -";
    } else {
        locTag = "<button onclick='openMapBrowser(&quot;"+this.location+"&quot;)' class='ui-btn ui-corner-all ui-icon-arrow-r ui-btn-icon-right ui-btn-inline ui-mini' style='background: GAINSBORO;'>Open Map</button>"
    }

    if (this.image === "") {
        imgTag = " -";
    } else {
        imgTag = "<div style='width: 178px; height: 100px; border:solid; margin:auto'>" +
            "<img style='width:100%; height:100%; object-fit: contain' src='"+this.image+"' onclick='window.open(this.src)' />" +
                "</div>";
    }

    div = "<li><div class='parentDiv'> <span class='nameHeading'><b id='nameforText'>" + this.category + ", logged on " +
        this.datetime + "</span></b>" + "</br></br><button class='DivExpand ui-btn ui-btn-inline ui-corner-all ui-icon-info ui-btn-icon-left' id='"+cbuttonID+"' style='background: GAINSBORO;'>Details</button>" + "<button class='deleteContact ui-btn ui-btn-inline ui-corner-all ui-icon-delete ui-btn-icon-left' id='"+dbuttonID+"' style='background: GAINSBORO;'>Delete</button>" +
        "<div class='childDiv_1' style='display:none;' id='" + divID + "'>" +
        "<div id='childDiv_2'>" + "<b>Employee Name: </b> "  + this.employeename + "</br></br><b>Employee Contact No: </b> " + this.contactno + "</br></br> <b>Category: </b>" + this.category +
        "</br></br> <b>Location: </b>" + locTag +
        "</br></br><b>Notes: </b>" + this.notes +
        "</br></br><b>Image: </b>" + imgTag +
        "</div>" + "</div>" + "</div><hr></li>";

    return div;
};

var updateList = function(){
    divID = 0;
    cbuttonID = 0;
    dbuttonID = 1000;
    var divList = document.getElementById("faultList");
    divList.innerHTML = "";

    // Clone array and use this clone to populate the div
    var faultsArraySorted = faultsArray.slice(0);

    // If user preference is set to newest first, reverse the cloned array before use.
    if (localStorage.getItem("sortPref") === "newest") {
        faultsArraySorted.reverse();
    }

    // Populate the fault list div
    for(var i=0, j=faultsArraySorted.length; i<j; i++){
        var fault = faultsArraySorted[i];
        divList.innerHTML += fault.divRow();
    }
};

// Open map in browser
var openMapBrowser = function(location) {
    cordova.InAppBrowser.open('http://maps.google.co.uk/maps?q=' + location, '_blank', 'location=yes');
};

/**
 * Takes a variable corresponding to a position within the faults array, then deletes (splices) that entry.
 * saveFaultList is then called to match the altered array to local storage.
 * @param int
 */
function removeFault (int) {

    // If the cloned array has been reversed to display newest first, the master array will need to be
    // reversed so that the correct fault record is deleted. The master array is then reversed back to
    // normal before being added into local storage.
    if (localStorage.getItem("sortPref") === "newest") {
        faultsArray.reverse();
        faultsArray.splice(int, 1);
        faultsArray.reverse();
        myControls = [];
        saveFaultList();
        updateList();
    } else {
        faultsArray.splice(int, 1);
        myControls = [];
        saveFaultList();
        updateList();
    }
}

/**
 * This function takes the id of the Details button, and matches it with the ID of the div it is held within. By
 * doing this we can use this button to extend the div when the user clicks the button, allowing for a neat list
 * of faults in faults.html.
 */
$(document).on('click','.DivExpand',function(){

    var id = $(this).prop("id");
    var divid;
//Now it matches the button to the DIV
    for (var i = 0; i < myControls.length; i++){
        if (myControls[i][1] == id){
            divid = myControls[i][0];
            break;
        }
    }

// Toggles the DIV
    $("#" + divid).slideToggle('fast');
});

/**
 * This function will grab the id of the delete button. As each delete button ID starts from 1001, the ID is decremented
 * by 1001 to give the div ID that it corresponds to. This allows the removeFault function to delete the fault.
 */
$(document).on('click','.deleteContact',function(){
    var response = confirm("Are you sure you want to delete this fault?");

    if (response) {
        var divid = $(this).prop("id");
        divid = divid - 1001;

        removeFault(divid);
    }
});
